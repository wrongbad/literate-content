define('notebook/main', 
        function(require, exports, module) {
        

const xhttp = require('lit/xhttp');
const browser = require('notebook/browser');
const executor = require('notebook/executor');
const notebook = require('notebook/notebook');


function sel(sel)
{   return document.querySelector(sel);
}  
function selAll(sel)
{   return document.querySelectorAll(sel);
}


var uname = sel('#username');
var pword = sel('#password');

var user = null;
var dirty = false;

function onClick(select, func)
{   sel(select).onclick = (e)=>!func() && false;
}


function docpath()
{   var path = window.location.pathname;
    path = path.replace(/^\/*(.*?)\/*$/, (m,m1)=>m1);
    if(path.length == 0) { path = 'home'; }
    if(!path.match(/.*\.[\w-_]+$/))
    {   path += '/index.lit';
        path = path.replace('//', '/');
    }

    let displayPath = path.replace('/index.lit', '');
    document.title = (dirty?'*':'')+displayPath;
    for(let elem of selAll('.doc-name'))
    {   elem.innerText = displayPath;
    }
    return path;
}


function setUser(u)
{   user = u;
    if(user)
    {   sel('#login-bar').classList.add('hidden');
        sel('#show-user').innerText = user.username;
        sel('#logout').classList.remove('hidden');
        uname.disabled = true;
        pword.disabled = true;
        popBar('#user-bar');
    }
    else
    {   sel('#login-bar').classList.remove('hidden');
        sel('#show-user').innerText = 'login';
        sel('#logout').classList.add('hidden');
        uname.disabled = false;
        pword.disabled = false;
    }
    setDirty(dirty); // update display
}
async function checkUser()
{   let resp = await xhttp.get('/~user/~whoami', {});
    try
    {   let data = JSON.parse(resp.text)
        setUser(data.user);
    } catch(e) {}
}


function setDirty(d)
{   dirty = d;
    if(dirty)
    {   sel('#save').classList.remove('hidden');
    }
    else
    {   sel('#save').classList.add('hidden');
    }
    docpath();
}


async function load()
{   var path = docpath();
    var resp = await xhttp.get('/d/'+path);
    if(resp.status == 200) {}
    else if(resp.status == 404) { resp.text = ''; }
    else
    {   resp.text = '['+resp.status+'] '+resp.text;
    }
    if(resp.status != 200)
    {   pushBar('#doc-bar');
        showPage('#browser');
    }
    notebook.load(path, resp.text);
    setDirty(false);
}
async function save()
{   if(!user) { return showLogin(); }
    var resp = await xhttp.put('/d/'+docpath(),
    {   filedata: await notebook.save(),
    });
    if(resp.status == 200)
    {   setDirty(false);
        popBar(); showPage(null);
    }
}
onClick('#save', save);


function addCellHooks(cell)
{   cell.editor.session.on('change', function()
    {   setDirty(true);
    });
    cell.editor.commands.addCommand(
    {   name: 'save',
        bindKey: {win: 'Ctrl-S', 'mac': 'Cmd-S'},
        exec: ()=>{ save() },
    });
}


async function login()
{   let resp = await xhttp.post('/~user/~login',
    {   username: uname.value, 
        password: pword.value
    });
    try
    {   let data = JSON.parse(resp.text)
        setUser(data.user);
    } catch(e) {}
    if(!user)
    {   pword.focus();
    }
}
async function logout()
{   let resp = await xhttp.post('/~user/~logout', {});
    if(resp.status == 200)
    {   setUser(null);
    }
}
function showLogin()
{   pushBar('#user-bar');
    uname.focus();
}

onClick('#login', login);
onClick('#logout', logout);


var bars = [sel('#home-bar')];
function pushBar(name)
{   let page = sel(name);
    let already = bars.indexOf(page);
    if(already >= 0)
    {   while(bars.length-1 > already)
        {   popBar();
        }
        return;
    }
    let prevPage = bars[bars.length-1];
    page.classList.remove('offright');
    prevPage.classList.add('offleft');
    sel('#head-left').classList.remove('hidden');
    bars.push(page);

    page.focus();
    if(name == '#user-bar' && !uname.disabled)
    {   uname.focus();
    }
}
function popBar(ifOnPage)
{   let page = bars[bars.length-1];
    if(ifOnPage && sel(ifOnPage) != page) { return; }
    let prevPage = bars[bars.length-2];
    if(!prevPage) { return; }
    prevPage.classList.remove('offleft');
    page.classList.add('offright');
    bars.pop();
    if(bars.length == 1)
    {   sel('#head-left').classList.add('hidden'); 
    }
    prevPage.focus();
}


var livePage = null;
function showPage(name)
{   var elem = sel(name);
    if(elem !== livePage)
    {   if(livePage) { livePage.classList.add('hidden'); }
        if(elem) { elem.classList.remove('hidden'); }
        livePage = elem;
    }
    let h = elem ? elem.scrollHeight : 0;
    sel('#page-container').style.height = h+'px';
}


onClick('#home-bar .doc-name', ()=>{pushBar('#doc-bar'); showPage('#browser')});
onClick('#show-user', ()=>pushBar('#user-bar'));
onClick('#head-left', ()=>{popBar(); showPage(null)});
onClick('#show-delete', ()=>showPage('#delete-doc'));


async function confirmDelete()
{   if(!user) { return showLogin(); }
    var resp = await xhttp.delete('/d/'+docpath());
    if(resp.status == 200)
    {   window.location = '/home';
        showPage('#browser');
    }
}

onClick('#delete-no', ()=>showPage('#browser'));
onClick('#delete-yes', ()=>confirmDelete());


exports.start = function()
{   checkUser();
    notebook.addCellHooks(addCellHooks);
    browser.init(sel('#browser'));
    showPage(null);
    load();

    document.addEventListener('keydown', function(e)
    {   let mac = window.navigator.platform.match('Mac');
        let mod = (mac ? e.metaKey : e.ctrlKey);
        if (mod && (e.which == 83 || e.which == 115) || e.which == 19)
        {   save();
            e.preventDefault();
        }
    }, false);

    let ifEnter = callback => event =>
        (event.key === 'Enter') ? callback() : null;
    uname.addEventListener('keyup', ifEnter(()=>pword.focus()));
    pword.addEventListener('keyup', ifEnter(()=>login()));
}

});