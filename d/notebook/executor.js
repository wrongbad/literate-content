define('notebook/executor',
        function(require, exports, module) {


var worker = {};
worker.hostChannel = new MessageChannel();
worker.hostPort = worker.hostChannel.port1;
var cells = {};
var enabled = false;
var cellHideCode = ()=>{};
var postRun = ()=>{};


function startWorker()
{   let workerFrame = document.getElementById('worker');
    if(!workerFrame) { console.log('no worker frame!'); return; }

    // TODO Let worker create the ports
    window.addEventListener('message', (e) =>
    {   if(e.data.do === 'workerReady')
        {   workerFrame.contentWindow.postMessage(
                {do: 'setPorts'}, '*', [worker.hostChannel.port2]);
        }
    });
    worker.hostPort.onmessage = (message) =>
    {   let d = message.data;
        if(d.do === 'resultReady')
        {   let cell = cells[d.cellId];
            let showView = d.viewer!='lit/default' ? d.viewer : null;
            setDepLinks(cell, d.deps, showView);
            loadViewer(cell, d.viewer);
        }
    };
}
startWorker();


function setDepLinks(cell, links, view)
{   cell.depLinks.innerHTML = '';
    let link = (t, url)=>{
        let a = document.createElement('a');
        a.href = url;
        a.innerText = t;
        cell.depLinks.appendChild(a);
    }
    for(let l of links)
    {   link(l, '/'+l+'.js');
    }
    if(view)
    {   if(view.indexOf('/')<0) { view += '/view'; }
        link(view, '/'+view+'.html');
    }
    let using = cell.depLinks.previousSibling;
    let hide = cell.depLinks.childElementCount == 0;
    using.classList.toggle('hidden', hide);
    cell.editorFooter.classList.toggle('hidden', hide);
}


function setResult(cell, newResult)
{   cell.container.removeChild(cell.resultNode);
    if(!newResult)
    {   newResult = document.createElement('div');
        newResult.classList.add('result');
    }
    cell.container.appendChild(newResult)
    cell.resultNode = newResult;
}


function loadViewer(cell, viewerPath)
{   let url = viewerPath;
    if(url.split(/\/+/).length<2) { url = url + '/view'; }

    // reset iframe every time
    // if(cell.viewerUrl === url) { return; }

    let resultNode = document.createElement('iframe');
    resultNode.classList.add('result');
    resultNode.sandbox = 'allow-scripts';
    resultNode.style.height = '30px';
    resultNode.tabIndex = -1;

    // TODO Let viewer initiate message handshake
    const workchan = new MessageChannel();
    const hostchan = new MessageChannel();
    let firstLoad = true;
    resultNode.onload = event => { if(firstLoad)
    {   // apparently firefox is mega dumb and
        // scrambles the ordering of the ports array
        // INTERMITTENTLY! WTF!
        // so we send two separate messages
        resultNode.contentWindow.postMessage(
            {do:'setHost'}, '*', [hostchan.port2]);
        resultNode.contentWindow.postMessage(
            {do:'setWork'}, '*', [workchan.port2]);
        firstLoad = false;
    }};
    hostchan.port1.onmessage = event =>
    {   if(event.data.do === 'setHeight')
        {   resultNode.classList.remove('noanimate');
            let h = event.data.height | 0;
            if(h > 2000) { h = 2000; }
            if(h < 0) { h = 0; }
            resultNode.style.height = h+'px';
        }
        if(event.data.do === 'setLocation')
        {   let url = event.data.url
            // TODO popup to confirm navigation
            // !!!!TODO sanitize js injection!!!!
            window.location = url;
        }
    }

    resultNode.src = '/d/'+url+'.html';
    setResult(cell, resultNode);

    let message = {};
    message.do = 'viewReady';
    message.cellId = cell.id;
    message.viewer = viewerPath;
    worker.hostPort.postMessage(message, [workchan.port1]);

    cell.workChannel = workchan;
    cell.hostChannel = hostchan;
    cell.viewerUrl = url;
}


function cellAttributes(code)
{
    let attr = {};
    attr.interpreter = null;
    let args = code.match(/^\/\/\/.*/);
    if(args) for(let f of args[0].split(/\s+/))
    {   if(f.startsWith('>>'))
        { attr.rawView = f.substr(2); }
        else if(f.startsWith('>'))
        { attr.view = f.substr(1); }
        else if(f.startsWith('^'))
        { attr.view = ''; } // TODO - view for upload
    }
    attr.length = args ? args[0].length : 0;
    attr.args = args ? args[0] : '';
    return attr;
}


var modelist = ace.require('ace/ext/modelist');
async function runCell(cell, userDirect)
{
    if(!enabled) { return; }

    let code = cell.editor.session.getValue();
    let attr = cellAttributes(code);

    let hide = !!attr.rawView;
    cellHideCode(cell, hide);

    if(!hide)
    {   cell.resultNode.classList.add('noanimate');
        cell.resultNode.style.height = '10px';
    }
    let view = attr.rawView || attr.view;

    if(view === '')
    {   // > to null
        setResult(cell, null);
        setDepLinks(cell, []);
        let ext = attr.args.match(/\.\w+$/);
        if(ext)
        {   let mode = modelist.getModeForPath(ext[0]).mode;
            cell.editor.session.setMode(mode);
        }
    }
    else if(view)
    {   code = code.substr(attr.length);
        loadViewer(cell, view);
        cell.hostChannel.port1.postMessage({value: code});
        let mode = modelist.getModeForPath(view.replace('/','.')).mode;
        cell.editor.session.setMode(mode);
        setDepLinks(cell, [], view);
    }
    else
    {   let message = {};
        message.do = 'eval';
        message.cellId = cell.id;
        message.code = code;
        message.attr = attr;
        worker.hostPort.postMessage(message, []);
        let ext = attr.args.match(/\.[\w+]$/);
        let mode = modelist.getModeForPath(ext ? ext[0] : '.js').mode;
        cell.editor.session.setMode(mode);
    }

    postRun(cell, userDirect);
}


exports.addCellHooks = function(cell)
{   cells[cell.id] = cell;

    cell.editor.commands.addCommand(
    {   name: 'run',
        bindKey: {win: "Ctrl-Enter", mac: "Cmd-Enter"},
        exec: ()=>{ runCell(cell, true) },
        readOnly: true
    });

    cell.editorOverlay.onclick = () => runCell(cell);
}
exports.runCell = runCell;
exports.setEnabled = (e) => { enabled = e };
exports.setHideFunc = (cb) => { cellHideCode = cb };
exports.setPostRun = (cb) => { postRun = cb };

});
