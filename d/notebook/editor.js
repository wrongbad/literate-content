define('notebook/editor', 
        function(require, exports, module) {
        
const xhttp = require('lit/xhttp');
const notebook = require('notebook/notebook');
const browser = require('notebook/browser');

function sel(sel)
{   return document.querySelector(sel);
}  
function selAll(sel)
{   return document.querySelectorAll(sel);
}   

var save = sel('#save');
var editBar = sel('#edit-bar');
var loginBar = sel('#login-bar');
var uname = sel('#username');
var pword = sel('#password');
var login = sel('#login');
var logout = sel('#logout');

var user = null;
var bars = [sel('#home-bar')];
var dirty = false;
var livePage = null;

var viewer = {};

function onClick(select, func)
{   sel(select).onclick = (e)=>!func() && false;
}

function docpath()
{   var path = window.location.pathname;
    path = path.replace(/^\/*(.*?)\/*$/, (m,m1)=>m1);
    if(path.length == 0)
    {   document.title = (dirty?'*':'')+'literate.af';
        path = 'home';
    }
    else
    {   document.title = (dirty?'*':'')+path;
    }
    if(!path.match(/.*\.[\w-_]+$/))
    {   path += '/index.lit';
        path = path.replace('//', '/');
    }
    // use resolved path here
    for(let elem of selAll('.doc-name'))
    {   elem.innerText = path;
    }
    return path;
}

function ifEnter(callback)
{   return function(event)
    {   if(event.key === "Enter")
        {   callback();
        }
    }
}

function setUser(u)
{   user = u;
    if(user)
    {   loginBar.classList.add('hidden');
        sel('#show-user').innerText = user.username;
        // sel('#show-login').classList.add('hidden');
        sel('#logout').classList.remove('hidden');
        viewer.popBar('#user-bar');
    }
    else
    {   loginBar.classList.remove('hidden');
        sel('#show-user').innerText = 'login';
        sel('#logout').classList.add('hidden');
    }
    setDirty(dirty); // update display
}
async function checkUser()
{   let resp = await (await xhttp).get('/a/whoami', {});
    try
    {   let data = JSON.parse(resp.text)
        setUser(data.user);
    } catch(e) {}
}

function setDirty(d)
{   dirty = d;
    if(dirty)
    {   save.classList.remove('hidden');
    }
    else
    {   save.classList.add('hidden');
    }
    docpath();
}

function setupEditor(div)
{   var editor = ace.edit(div);
    editor.setOptions(
    {   printMargin: false,
        maxLines: 2000,
        minLines: 1,
        useSoftTabs: true,
        wrap: true,
        theme: 'ace/theme/solarized_light',
        fontSize: "15px",
        // showLineNumbers: false,
        highlightActiveLine: false,
        showGutter: false,
    });
    editor.getSession().on('change', function()
    {   setDirty(true);
    });
    editor.setAutoScrollEditorIntoView();
    editor.commands.addCommand(
    {   name: 'save',
        bindKey: {win: "Ctrl-S", "mac": "Cmd-S"},
        exec: ()=>{ viewer.save() },
    });
    return editor;
}

viewer.start = function()
{   viewer.load();
    checkUser();
    browser.init(sel('#browser'));

    document.addEventListener("keydown", function(e)
    {   let mac = window.navigator.platform.match("Mac");
        let mod = (mac ? e.metaKey : e.ctrlKey);
        if (mod && (e.which == 83 || e.which == 115) || e.which == 19)
        {   viewer.save();
            e.preventDefault();
        }
    }, false);

    uname.addEventListener("keyup", ifEnter(()=>pword.focus()));
    pword.addEventListener("keyup", ifEnter(()=>viewer.login()));

    viewer.showPage(null);
}

viewer.load = async function()
{   var path = docpath();
    var resp = await (await xhttp).get('/d/'+path);
    if(resp.status == 200) {}
    else if(resp.status == 404) { resp.text = ''; }
    else
    {   resp.text = '['+resp.status+'] '+resp.text;
    }
    (await notebook).load(path, resp.text, setupEditor);
    setDirty(false);
}
viewer.save = async function()
{   if(!user) { return viewer.showLogin(); }
    var resp = await (await xhttp).put('/d/'+docpath(),
    {   filedata: await (await notebook).save(),
    });
    if(resp.status == 200)
    {   setDirty(false);
    }
}
onClick('#save', viewer.save);

viewer.login = async function()
{   let resp = await (await xhttp).post('/a/login',
    {   username: uname.value, 
        password: pword.value
    });
    try
    {   let data = JSON.parse(resp.text)
        setUser(data.user);
    } catch(e) {}
    if(!user)
    {   pword.focus();
    }
}
viewer.logout = async function()
{   let resp = await (await xhttp).post('/a/logout', {});
    if(resp.status == 200)
    {   setUser(null);
    }
}
viewer.showLogin = function()
{   viewer.pushBar('#user-bar');
    uname.focus();
}

onClick('#login', viewer.login);
onClick('#logout', viewer.logout);

viewer.pushBar = function(name)
{   let page = sel(name);
    let already = bars.indexOf(page);
    if(already >= 0)
    {   while(bars.length-1 > already)
        {   viewer.popBar();
        }
        return;
    }
    let prevPage = bars[bars.length-1];
    page.classList.remove('offright');
    prevPage.classList.add('offleft');
    sel('#head-left').classList.remove('hidden');
    bars.push(page);
    page.focus();
}
viewer.popBar = function(ifOnPage)
{   let page = bars[bars.length-1];
    if(ifOnPage && sel(ifOnPage) != page) { return; }
    let prevPage = bars[bars.length-2];
    prevPage.classList.remove('offleft');
    page.classList.add('offright');
    bars.pop();
    if(bars.length == 1)
    {   sel('#head-left').classList.add('hidden'); 
    }
    prevPage.focus();
}

onClick('#home-bar .doc-name', ()=>viewer.pushBar('#doc-bar'));
onClick('#show-user', ()=>viewer.pushBar('#user-bar'));
onClick('#head-left', ()=>viewer.popBar());

viewer.showPage = function(name)
{   var elem = sel(name);
    if(!elem) { elem = sel('#browser'); }
    if(elem !== livePage)
    {   if(livePage) { livePage.style.height = '0px'; }
        if(elem) { elem.style.height = 'fit-content'; }
        livePage = elem;
    }
    elem.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

onClick('#show-delete', ()=>viewer.showPage('#delete-doc'));

viewer.confirmDelete = async function(confirmed)
{   if(confirmed)
    {   if(!user) { return viewer.showLogin(); }
        var resp = await (await xhttp).delete('/d/'+docpath());
        if(resp.status == 200)
        {   window.location = '/home';
        }
    }
    viewer.showPage(null);
}

onClick('#delete-no', ()=>viewer.confirmDelete(false));
onClick('#delete-yes', ()=>viewer.confirmDelete(true));

module.exports = viewer;

});