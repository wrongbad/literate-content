define('notebook/worker', 
        function(require, exports, module) {
        

var fake_exports = {};
var fake_module = {exports: fake_exports};
var reqTable = {};
function prefetch(paths)
{   return new Promise(resolve =>
        require(paths, function(x)
        {   for(let i=0 ; i<paths.length ; i++)
            {   reqTable[paths[i]] = arguments[i];
            }
            resolve(arguments);
        })
    );
}
function fake_require(path)
{   return reqTable[path];
}


var hostPort = {};
var cellPorts = {};
var cellViewers = {};
var cellPending = {};


let prevResult = async ()=>{};
function setPorts(event)
{   hostPort = event.ports[0];
    hostPort.onmessage = async function(event)
    {   await prevResult;
        prevResult = exports.hostMessage(event);
    }
}


function viewReady(event)
{   let cellId = event.data.cellId;
    let viewer = event.data.viewer;
    cellPorts[cellId] = event.ports[0];
    cellPorts[cellId].onmessage = e =>
        console.log('from ',cellId,' ',e);
    cellViewers[cellId] = viewer;
    if(cellPending[cellId])
    {   if(cellPending[cellId].viewer == viewer)
        {   sendResultCell(cellPending[cellId]);
            delete cellPending[cellId];
        }
        else
        {   console.log('Dropping: ', cellPending[cellId]);
        }
    }
}


function sendResultCell(result)
{   cellPorts[result.cellId].postMessage(result);
}

function resultReady(result)
{   
    try
    {   hostPort.postMessage(result);
    } catch(e)
    {   result.value = undefined;
        hostPort.postMessage(result);
    }
    
    let port = cellPorts[result.cellId];
    if(cellViewers[result.cellId] != result.viewer || !port)
    {   cellPending[result.cellId] = result;
    }
    else
    {   sendResultCell(result);
    }
}

function makeResult(value, attr)
{   
    if(typeof value === 'function')
    {   value = {'function': String(value)};
    }
    let viewer = attr.view;
    if(typeof value !== 'undefined')
    {   viewer = viewer || value._litviewer;
    }
    viewer = viewer || 'lit/default';
    return {
        do: 'resultReady',
        viewer: viewer,
        value: value,
    };
}


async function evalCode(event)
{   let code = event.data.code;
    let cellId = event.data.cellId;
    let attr = event.data.attr;
    
    // must be global
    _scope_ = {
        // $cell: cellId, TODO message port
        require: fake_require,
        module: fake_module,
        exports: fake_exports,
    };

    code = `with (_scope_) {\n${code}\n/**/}`;

    let deps = [];
    let reqgex = /(?:^|[^\.\w])require\s*\(\s*['"]([\w\/]+)['"]\s*\)/g;
    for(let m of code.matchAll(reqgex))
    {   if(!deps.includes(m[1]))
        {   deps.push(m[1]);
        }
    }
    try
    {   await prefetch(deps);
        var value = eval.call(null, code);
    }
    catch(e)
    {   value = String(e);
    }
    let result = makeResult(value, attr);
    result.cellId = cellId;
    result.deps = deps;
    resultReady(result);
}


exports.hostMessage = async function(event)
{   try 
    {   if(event.data.do === 'eval')
        {   return await evalCode(event);
        }
        if(event.data.do === 'viewReady')
        {   return viewReady(event);
        }
        if(event.data.do === 'setPorts')
        {   return setPorts(event);
        }
    }
    catch(e)
    {   console.log(e);
    }
}

});