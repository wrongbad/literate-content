define('notebook/browser', 
        function(require, exports, module) {
        

const xhttp = require('lit/xhttp');
var container = null;


async function load()
{   let path = window.location.pathname;
    let resp = await xhttp.get('/ls'+path);
    if(resp.status == 200)
    {   let items = JSON.parse(resp.text);
        for(let i of items.items)
        {   let a = document.createElement('a');
            a.href = i[0]=='/' ? i : '/'+i;
            a.innerText = i;
            a.style.display = 'block';
            container.appendChild(a);
        }
    }
}


exports.init = function(elem, location)
{   container = elem;
    load();
};


});