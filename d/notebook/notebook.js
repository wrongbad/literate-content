define('notebook/notebook',
        function(require, exports, module) {


var modelist = ace.require('ace/ext/modelist');
var executor = require('notebook/executor');
var parent = document.querySelector('#notebook');
var path;

var setValueHack = false;
var viewerMap = {};

var nextCellId = 1;
var cells = [];
var cellHooks = [executor.addCellHooks];

var worker;


const mac = window.navigator.platform.match("Mac");
// let platformHints = 'ctrl-enter to run';
// if(mac) { platformHints = 'cmd-enter to run'; }
let platformHints = '';


function getCell(id)
{   for(let c of cells) if(c.id === id)
    {   return c;
    }
    return null;
}
function cellAfter(cell)
{   let i = cells.indexOf(cell);
    if(i < 0) { return null; }
    return cells[i+1];
}
function cellBefore(cell)
{   let i = cells.indexOf(cell);
    if(i < 0) { return null; }
    return cells[i-1];
}


function newCell(after, before)
{   let cell = {};

    cell.id = 'cell'+(nextCellId++);
    let template = document.getElementById('cell-template');
    let cellNode = template.cloneNode(true);
    cellNode.id = '';
    cellNode.classList.remove('hidden');
    cell.container = cellNode;
    cell.expandNode = cellNode.querySelector('.expand');
    cell.editorNode = cellNode.querySelector('.editor');
    cell.editorWrapper = cellNode.querySelector('.editor-wrapper');
    cell.resultNode = cellNode.querySelector('.result');
    cell.editorFooter = cellNode.querySelector('.editor-footer');
    cell.editorHints = cellNode.querySelector('.editor-hints');
    cell.depLinks = cellNode.querySelector('.dep-links');
    cell.editorOverlay = cellNode.querySelector('.editor-overlay');

    cell.expandNode.onclick = function()
    {   cellHideCode(cell, false);
        var row = cell.editor.session.getLength();
        cell.editor.gotoLine(row, 999);
        cell.editor.focus();
        return false;
    };

    cell.editorHints.innerText = platformHints;
    cell.codeHidden = false;
    cell.codeHiddenSave = false;

    let beforeNode = null;
    if(after) { beforeNode = after.container.nextSibling; }
    if(before) { beforeNode = before.container; }
    parent.insertBefore(cellNode, beforeNode);

    let editor = ace.edit(cell.editorNode);
    cell.editor = editor;

    editor.setOptions(
    {   printMargin: false,
        maxLines: 2000,
        minLines: 1,
        useSoftTabs: true,
        wrap: true,
        theme: 'ace/theme/gruvbox',
        fontSize: "15px",
        highlightActiveLine: false,
        showGutter: false,
    });
    editor.setAutoScrollEditorIntoView(true);

    editor.session.on('change', function(change)
    {   checkCell(cell, change);
    });

    editor.commands.removeCommand('find');
    // let ext = path.endsWith('.lit') ? '.js' : path;
    let ext = path;
    let mode = modelist.getModeForPath(ext).mode;
    editor.session.setMode(mode);
    editor.session.setUseWorker(false);

    editor.session.setValue('///');
    editor.gotoLine(1, 3);

    let newIndex = cells.length;
    if(before) { newIndex = cells.indexOf(before); }
    if(after) { newIndex = cells.indexOf(after)+1; }
    cells.splice(newIndex, 0, cell);

    // trying to enable clicking on results
    // to bump cursor back into editable
    editor.on('focus', function()
    {   cellHideCode(cell, false, true);
    });
    editor.on('blur', function()
    {   cellRestoreHidden(cell);
    });

    // when capture arrow-key events
    // ace only exposes the cursor position after
    // handling the arrow key event
    // so we do this hackery to capture the cursor position
    // from the previous event loop pass
    var lastCursor = 0;
    editor.selection.on("changeCursor", ()=>{
        let c = editor.selection.getCursor();
        setTimeout(()=>{ lastCursor=c }, 0);
    });

    editor.textInput.getElement().addEventListener("keydown", (e)=>{
        // let c = editor.selection.getCursor();
        let c = lastCursor;
        if(e.keyCode === 40) // down
        {   let n = editor.session.getDocument().getLength();
            if(c.row === n-1)
            {   let next = cellAfter(cell);
                if(next)
                {   next.editor.gotoLine(1, c.column);
                    next.editor.focus();
                    e.preventDefault();
                }
            }
        }
        else if(e.keyCode === 38) // up
        {   if(c.row === 0)
            {   let next = cellBefore(cell);
                if(next)
                {   let n = next.editor.session.getDocument().getLength();
                    next.editor.gotoLine(n, c.column);
                    next.editor.focus();
                    e.preventDefault();
                }
            }
        }
        else if(e.keyCode === 8) // backspace
        {   if(c.row !== 0 || c.column !== 0) { return; }
            e.preventDefault();
            let prev = cellBefore(cell);
            if(!prev) { return; }
            let text = cellText(prev);
            if(text.match(/[^\s]$/))
            {   removeCell(prev);
                setCellText(cell, text + cellText(cell));
                let lines = text.split('\n');
                cell.editor.gotoLine(lines.length,
                    lines[lines.length-1].length);
                cell.editor.focus();
                return;
            }
            text = text.substr(0,text.length-1);
            setCellText(prev, text);
        }
    });

    for(let f of cellHooks) { f(cell); }

    return cell;
}


function cellRan(cell, userDirect)
{
    if(userDirect)
    {   let next = cellAfter(cell);
        if(next)
        {   var row = next.editor.session.getLength();
            next.editor.gotoLine(row, 999);
            next.editor.focus();
        }
    }
}


function cellRestoreHidden(cell)
{   cellHideCode(cell, cell.codeHiddenSave);
}
function cellHideCode(cell, hidden, temp)
{   // console.log(cell.id, hidden, temp);
    if(!temp) { cell.codeHiddenSave = hidden; }
    if(cell.codeHidden === hidden) { return; }
    let e = cell.editor;
    let h = e.session.getScreenLength();
    h = (h || 1) * (e.renderer.lineHeight || 20);
    if(hidden)
    {   cell.editorOverlay.classList.add('hidden');
        cell.editorWrapper.style.overflow = 'hidden';
        cell.editorWrapper.style.height = h+'px';
        setTimeout(()=>{ if(cell.codeHidden)
        {   cell.editorWrapper.classList.remove('noanimate');
            cell.editorWrapper.classList.add('collapse');
            cell.editorWrapper.style.height = '';
            cell.editorFooter.style.height = '0px';
        }}, 0);
        setTimeout(()=>{ if(cell.codeHidden)
        {   cell.expandNode.classList.remove('hidden');
        }}, 100);
    }
    else
    {   cell.expandNode.classList.add('hidden');
        cell.editorWrapper.classList.remove('noanimate');
        cell.editorWrapper.classList.remove('collapse');
        cell.editorWrapper.style.height = h+'px';
        cell.editorFooter.style.height = '20px';
        setTimeout(()=>{ if(!cell.codeHidden)
        {   cell.editorOverlay.classList.remove('hidden');
        }}, 100);
        setTimeout(()=>{ if(!cell.codeHidden)
        {   cell.editorWrapper.classList.add('noanimate');
            cell.editorWrapper.style.overflow = '';
            cell.editorWrapper.style.height = '';
        }}, 300);
    }
    cell.codeHidden = hidden;
}


function cellText(cell)
{   return cell.editor.session.getValue()
}
function setCellText(cell, newText)
{   setValueHack = true;
    cell.editor.session.setValue(newText);
    setValueHack = false;
    cell.editor.session.getUndoManager().reset();
}


function removeCell(cell)
{   let index = cells.indexOf(cell);
    cells.splice(index, 1);
    setValueHack = true;
    parent.removeChild(cell.container);
    setValueHack = false;
}

function reflow(cell, split, col)
{   let topCell = cellBefore(cell);
    if(!topCell && split.length == 1) { return; }
    let editor = cell.editor;
    let pos = editor.session.getSelection().getCursor();
    let i = 0;
    let mergeFocus = editor;
    if(split[i].length == 0) { i++; }
    else if(topCell)
    {   let topText = cellText(topCell);
        pos.row = topText.split('\n').length+1;
        mergeFocus = topCell.editor;
        setCellText(topCell, topText+'\n'+split[i]);
        i++;
    }
    while(i < split.length-1)
    {   let c = newCell(null, cell);
        cellHideCode(c, false);
        let s = split[i++];
        setCellText(c, topCell ? '///'+s : s);
    }
    if(i < split.length)
    {   let text = split[i];
        if(i>0) { text = '///'+text; }
        else { text = text.substr(1); }
        setCellText(cell, text);
        pos.row = 1;
        mergeFocus = editor;
        cell.codeHiddenSave = false;
    }
    else
    {   removeCell(cell);
    }
    setTimeout(function()
    {   mergeFocus.gotoLine(pos.row, col);
        mergeFocus.focus();
    });
}


function checkCell(cell, change)
{
    if(setValueHack) { return; }
    if(!path.endsWith('.lit')) { return; }

    let val = cell.editor.session.getValue();
    if(!val.length) { return; }
    let split = val.split(/[\r\n]*^\/\/\//m);
    if(split.length < 2 || ('///'+split[1]) !== val)
    {   let col = change.end.column;
        if(change.action === 'remove') { col = change.start.column; }
        reflow(cell, split, col);
    }
}


exports.load = async function(filepath, content)
{   path = filepath;
    parent.innerHTML = '';
    editors = [];
    executor.setEnabled(path.endsWith('.lit'));
    executor.setHideFunc(cellHideCode);
    executor.setPostRun(cellRan);

    let contentSplit = (content).split(/[\r\n]*^\/\/\//m);
    let prefix = '';

    for(let cellText of contentSplit)
    {   if(cellText)
        {   let cell = newCell();
            setCellText(cell, prefix+cellText);
            cellHideCode(cell, false);
            executor.runCell(cell);
        }
        prefix = '///';
    }
    if(cells.length === 0)
    {   cellHideCode(newCell(), false);
    }
    cells[cells.length-1].editor.focus();
}


exports.save = async function()
{   let content = '';
    let prefix = '';
    for(let cell of cells)
    {   content += prefix + cell.editor.session.getValue();
        prefix = '\n';
    }
    return content;
}
//
exports.addCellHooks = function(func)
{   cellHooks.push(func);
}

});
