var name = 'wasm/test';
define(name, function(require, exports, module) {

Module.locateFile = (file) => {
    let pre = name.match(/([^\/]*\/)+/);
    return pre ? '/d/'+pre[0]+file : '/d/'+file;
};
