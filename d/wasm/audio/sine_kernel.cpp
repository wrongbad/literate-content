
#include "wasm/audio/sine_kernel.h"

#include "emscripten/bind.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(sine_kernel)
{   class_<SineKernel>("Kernel")
        .constructor<float>()
        .function("process", &SineKernel::process, allow_raw_pointers())
        .class_function("id", &SineKernel::id);
}
