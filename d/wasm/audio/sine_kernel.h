#pragma once

#include <cmath>
#include <string>
#include <cstdint>

class SineKernel
{
public:
    SineKernel(float srate)
    :   _srate(srate)
    {
    }
    void process(uintptr_t in_ptr, uintptr_t out_ptr, int chunk)
    {   float * out = (float*)out_ptr;
        for(int i=0 ; i<chunk ; i++)
        {   out[i] = ::sin(_phase * M_PI * 2);
            _phase += _freq / _srate;
            _phase -= _phase > 1;
        }
    }
    static std::string id() { return "sine_kernel"; }
private:
    float const _srate;
    float _phase = 0;
    float _freq = 100;
};
