
const audio_buffer_size = 128;
const sizeof_float = Float32Array.BYTES_PER_ELEMENT;
        
class AudioWorkletWrapper extends AudioWorkletProcessor
{   constructor()
    {   super();
        this._kernel = new Module.Kernel(sampleRate);
        this._buffer_ptr = Module._malloc(audio_buffer_size * sizeof_float);
        this._buffer = Module.HEAPF32.subarray( this._buffer_ptr / sizeof_float, 
            this._buffer_ptr / sizeof_float + audio_buffer_size );
    }
    process(inputs, outputs, parameters)
    {   this._kernel.process(0, this._buffer_ptr, audio_buffer_size);
        outputs[0][0].set(this._buffer);
        return true;
    }
}

registerProcessor(Module.Kernel.id(), AudioWorkletWrapper);
