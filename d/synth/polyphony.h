#pragma once

#include "synth/ring.h"

template<class Voice, int N>
class PolyphonyMux
{
public:
    template<class... Args>
    PolyphonyMux(Args const&... voiceArgs)
    :   _voices(voiceArgs...),
        _notes(-1)
    {
    }
    void noteOn(int note, int vel)
    {   _notes.shift(1);
        _voices.shift(1);
        _notes[0] = note;
        _voices[0].noteOn(note, vel);
        _count ++;
    }
    void noteOff(int note)
    {   for(int i=_count-1 ; i>=0 ; i--)
        {   if(_notes[i] != note) { continue; }
            _notes.bubble(i, _count-1);
            _count --;
            if(i >= N) { return; }
            int j = std::min(_count, N-1);
            _voices.bubble(i, j);
            if(j >= _count) { _voices[j].noteOff(); }
            else { _voices[j].noteOn(_notes[j], 111); } // TODO!!!
            return;
        }
    }
    void render(float * out, int samples)
    {   for(int i=0 ; i<N ; i++)
        {   _voices[i].render(out, samples);
        }
    }

private:
    Ring<Voice, N> _voices;
    Ring<int, 128> _notes;
    int _count = 0;
};
