#pragma once

#include <vector>
#include <algorithm>

// Array-style structure for fast shifts/rotates
template<class T, int N>
class Ring
{
public:
    template<class... Args>
    Ring(Args const&... args)
    {   _arr.reserve(N);
        for(int i=0 ; i<N ; i++)
        {   _arr.emplace_back(args...);
        }
    }
    
    T & operator[](int i) { return _arr[index(i)]; }
    
    T const& operator[](int i) const { return _arr[index(i)]; }
    
    // move all elements right by i
    void shift(int i) { _zero = index(N - i%N); }
    
    // move "i" to "to", shifting those in between
    void bubble(int i, int to)
    {   for(int j=i, dj=to>i?1:-1 ; j!=to ; j+=dj)
        {   std::swap((*this)[j], (*this)[j+dj]);
        }
    }
private:
    int index(int i) { return (_zero + i) % N; }

    std::vector<T> _arr;
    int _zero = 0;
};
