#pragma once

#include <cmath>
#include <iostream>

inline float midiFreq(int m)
{   static const float b = std::log(2.0) / 12.0; // +12 notes = 2x freq
    static const float a = 440 / std::exp(69 * b); // note 69 == 440 hz
    return a * std::exp(m * b);
}

inline int midiNote(float f)
{   static const float b = std::log(2.0) / 12.0; // +12 notes = 2x freq
    static const float a = 440 / std::exp(69 * b); // note 69 == 440 hz
    return std::log(f / a) / b;
}

class MidiControl
{
public:
    MidiControl()
    {   for(int i=0 ; i<128 ; i++)
        {   _ctl[i] = 64;
            _atouch[i] = 0;
        }
    }
    int operator[](int id) const { return _ctl[id]; }
    int & operator[](int id) { return _ctl[id]; }
    int atouch(int id) const { return _atouch[id]; }
    int & atouch(int id) { return _atouch[id]; }
private:
    int _ctl[128];
    int _atouch[128];
};

template<class NoteReader>
class MidiReader
{
public:
    MidiReader(MidiControl & controls, NoteReader & notes)
    :   _controls(controls),
        _notes(notes)
    {
    }

    void midi(int cmd, int d1 = 0, int d2 = 0)
    {
        if(cmd == 0x90 && d2 == 0) { cmd = 0x80; }
        if(cmd == 0x80)
        {   _controls.atouch(d1) = 0;
            _notes.noteOff(d1);
        }
        else if(cmd == 0x90)
        {   _controls.atouch(d1) = 127;
            _notes.noteOn(d1, d2);
        }
        else if(cmd == 0xA0)
        {   _controls.atouch(d1) = d2;
        }
        else if(cmd == 0xB0)
        {   _controls[d1] = d2;
        }
    }

private:
    MidiControl & _controls;
    NoteReader & _notes;
};
