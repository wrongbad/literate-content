#pragma once

#include <cmath>

#include "synth/midi.h"

class SynthVoice
{
public:
    SynthVoice(MidiControl const& control, float sampleRate)
    :   _control(&control),
        _srate(sampleRate)
    {
    }
    void noteOn(int key, int vel)
    {   _key = key;
        _vel = vel;
        _freq = midiFreq(key);
        _amp = 0.1;
        _phase = 0;
        _phase2 = 0;
        _time = 0;
        // _atouchSlow = vel;
        _atouchSlow = 0;
    }
    void noteOff()
    {   _amp = 0;
    }
    void render(float * out, int samples)
    {   
        using std::exp; using std::sin;
        if(_freq < 20 || (_amp == 0 && _amp1 == 0)) { return; }

        // midi control id mapping
        const int c1 = 21;
        const int c2 = 22;
        const int c3 = 23;
        const int c4 = 24;
        MidiControl const& ctl = *_control;

        int n = _srate / _freq / 2;

        float det = ctl[c3] * 0.0002;

        float dp = _freq / _srate * 2 * M_PI;
        float dp2 = _freq / _srate * 2 * M_PI * (2+det);
        // float inharm = ctl[c4] * 0.0001;
        float p = _phase * 2 * M_PI;
        float p2 = _phase2 * 2 * M_PI;
        for(int i=0 ; i<samples ; i++)
        {   _amp1 += std::min(_amp - _amp1, 0.0f) * 0.001;
            _amp1 += std::max(_amp - _amp1, 0.0f) * 0.01;
            _time ++;
            p += dp;
            p2 += dp2;
            
            _atouchSlow += (ctl.atouch(_key) - _atouchSlow) * 0.001;

            float et = _time * 0.001 * exp((127-ctl[c4]) * -0.1) + 1;    
            float b = exp(ctl[c1] * -0.03 + _key * 0.02 + _atouchSlow * -0.02 + 1.5) * et;
            float a = b * 10;

            // for(int j=1 ; j<std::min(n,32) ; j++)
            // {   _phase[j] += dp * j * std::sqrt(1 + inharm*j*j);
            //     if(_phase[j] > 2 * M_PI) { _phase[j] -= 2 * M_PI; }
            // }

            // float y = 0;
            // for(int j=1 ; j<std::min(n,32) ; j++)
            // {   y += _amp1 * a * sin(_phase[j]) * exp(-b*j);
            // }

            // float y2 = 0;
            // for(int j=1 ; j<std::min(n,32) ; j++)
            // {   y2 += _amp1 * a2 * sin(p2 * j) * exp(-b2*j);
            // }

            float y = _amp1 * a * (
                exp(-b) * sin(p)
                - exp(-(n+1)*b) * sin((n+1)*p)
                + exp(-(n+2)*b) * sin(n*p)
            ) / ( 1 - 2*exp(-b)*cos(p) + exp(-2*b) );

            // float b2 = exp(ctl[c2] * -0.03 + _key * 0.02) * et;
            float b2 = b * 2;
            // float a2 = b2 * 10;
            float a2 = a * ctl[c2] / 127.0;
        
            float y2 = _amp1 * a2 * (
                exp(-b2) * sin(p2)
                - exp(-(n+1)*b2) * sin((n+1)*p2)
                + exp(-(n+2)*b2) * sin(n*p2)
            ) / ( 1 - 2*exp(-b2)*cos(p2) + exp(-2*b2) );
            
            // float y = _amp * sin(p), y2 = 0;
            out[i] += ( y - y2 ) / 8.0f;
            // out[i] += y;
        }
        p = p * (0.5 / M_PI);
        p2 = p2 * (0.5 / M_PI);
        _phase = p - std::floor(p);
        _phase2 = p2 - std::floor(p2);
    }
private:
    MidiControl const* _control = nullptr;
    float _srate = 0;

    int _key = 0;
    int _vel = 0;
    int _time = 0;
    float _freq = 0;
    float _amp = 0;
    float _amp1 = 0;
    float _phase = 0;
    float _phase2 = 0;
    float _atouchSlow = 0;
    // float _phase[32] = {0};
    // float _phase2[32] = {0};
};
