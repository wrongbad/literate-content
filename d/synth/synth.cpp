
#include "synth/midi.h"
#include "synth/polyphony.h"
#include "synth/voice.h"

#include <cmath>
#include <string>
#include <cstdint>
    
class SynthKernel
{
public:
    using PolyMux = PolyphonyMux<SynthVoice, 8>;

    SynthKernel(float srate)
    :   _srate(srate)
    {
    }
    void process(uintptr_t input, uintptr_t output, size_t size)
    {   float * out = (float*)output;
        std::fill(out, out+size, 0);
        _polyMux.render(out, size);
    }
    void midi3(uint8_t m0, uint8_t m1, uint8_t m2)
    {   _midiReader.midi(m0, m1, m2);
    }
    static std::string id() { return "synth_kernel"; }
private:
    float const _srate;
    
    MidiControl _controls;
    PolyMux _polyMux { _controls, _srate };
    MidiReader<PolyMux> _midiReader { _controls, _polyMux };
};

#include "emscripten/bind.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(SynthKernel)
{   class_<SynthKernel>("Kernel") // using name Kernel for compat with js tail code
        .constructor<float>()
        .function("process", &SynthKernel::process)
        .function("midi3", &SynthKernel::midi3)
        .class_function("id", &SynthKernel::id);
}
