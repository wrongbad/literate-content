define('plot/make', function(require, exports, module) {
        
exports.domain = function(a,b) {
    return {
        map: func => ({ 
            _litviewer: 'plot/view',
            y: _.range(1001).map( x => func(x*(b-a)/1000+a) )
        })
    }
}

});