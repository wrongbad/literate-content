define('lit/xhttp', 
        function(require, exports, module) {
        

exports.get = function(url)
{   return new Promise(function(resolve, reject)
    {   let request = new XMLHttpRequest();
        request.onloadend = function()
        {   resolve({
                status: this.status,
                text: this.responseText
                // TODO more
            });
        };
        request.open("GET", url, true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send();
    });
}


exports.put = function(url, data)
{   return new Promise(function(resolve, reject)
    {   
        var formData = new FormData();
        for(key in data) if(data.hasOwnProperty(key))
        {   formData.append(key, data[key]);
        }
        let request = new XMLHttpRequest();
        request.onloadend = function()
        {   resolve({
                status: this.status,
                text: this.responseText
                // TODO more
            });
        };
        request.open("PUT", url, true);
        // request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send(formData);
    });
}


exports.post = function(url, data)
{   return new Promise(function(resolve, reject)
    {   
        var formData = new FormData();
        for(key in data) if(data.hasOwnProperty(key))
        {   formData.append(key, data[key]);
        }
        let request = new XMLHttpRequest();
        request.onloadend = function()
        {   resolve({
                status: this.status,
                text: this.responseText
                // TODO more
            });
        };
        request.open("POST", url, true);
        // request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send(formData);
    });
}


exports.delete = function(url)
{   return new Promise(function(resolve, reject)
    {   let request = new XMLHttpRequest();
        request.onloadend = function()
        {   resolve({
                status: this.status,
                text: this.responseText
                // TODO more
            });
        };
        request.open("DELETE", url, true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send();
    });
}

});