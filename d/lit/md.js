define('lit/md', 
        function(require, exports, module) {
        

// declares global 'markdown'
require('/d/rehost/markdown.min.js');

exports.eval = function(code)
{   return {
        html: markdown.toHTML(code),
        _litviewer: 'lit/views/html.html'
    };
}

});