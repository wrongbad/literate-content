var canvas;
var nkeys = 37; // 3 octave
var key0midi = 36; // c

var keys = [];

var midiOut = null;

function keyBlack(key)
{   key = (key + key0midi + 7) % 12;
    return (key + key/7|0) % 2;
}
var nwkeys = 0;
for(let k=0 ; k<nkeys ; k++)
{   nwkeys += keyBlack(k) ? 0 : 1;
}
var wkw = 35;
var bkw = wkw * 0.67;
var canvasw = nwkeys * wkw;
var canvash = 128;
for(let k=0,wk=0 ; k<nkeys ; k++)
{   let midi = key0midi + k;
    let sp = midi % 12;
    sp = (sp < 5 ? sp-2 : sp-8) * bkw * 0.09;
    let key = { 
        black: keyBlack(k),
        x: 0,
        y: 0,
        w: 0,
        h: canvash,
        i: k,
        midi: midi,
    };
    if(key.black)
    {   key.x = wk*wkw - bkw/2 + sp;
        key.w = bkw;
        key.h = canvash * 0.6;
    }
    else
    {   key.x = wk * wkw + 1;
        key.w = wkw - 2;
        wk++;
    }
    keys.push(key);
}

function keyAt(x, y)
{   x = x * canvasw / canvas.offsetWidth;
    y = y * canvash / canvas.offsetHeight;
    function inkey(key, x, y)
    {   return key.x < x && key.y < y
            && key.x + key.w > x && key.y + key.h > y;
    }
    for(let key of keys) if(key.black)
    {  if(inkey(key, x, y)) { return key.i; }
    }
    for(let key of keys) if(!key.black)
    {  if(inkey(key, x, y)) { return key.i; }
    }
    return -1;
}

function updateKeys()
{   for(let key of keys)
    {   if(key.sent === key.down) { continue; }
        let cmd = key.down ? 0x90 : 0x80;
        midiOut && midiOut([cmd, key.midi, 127]);
        key.sent = key.down;
    }
}

function touchStart(e)
{   console.log('TODO',e);
}
function touchMove(e)
{   console.log('TODO',e);
}
function touchEnd(e)
{   console.log('TODO',e);
}

var mouseDrag = 0;
function mouseDown(e)
{   var key = keyAt(e.offsetX, e.offsetY);
    for(let i=0 ; i<nkeys ; i++) { keys[i].down = 0; }
    if(key < 0) { return; }
    keys[key].down = 1;
    mouseDrag = 1;
    updateKeys();
}
function mouseMove(e)
{   if(!mouseDrag)  { return; }
    var key = keyAt(e.offsetX, e.offsetY);
    for(let i=0 ; i<nkeys ; i++) { keys[i].down = 0; }
    if(key < 0) { return; }
    keys[key].down = 1;
    updateKeys();
}
function mouseUp(e)
{   for(let i=0 ; i<nkeys ; i++) { keys[i].down = 0; }
    mouseDrag = 0;
    updateKeys();
}

function draw()
{   let ctx = canvas.getContext("2d");
    function drawkey(key)
    {   ctx.beginPath();
        ctx.rect(key.x, key.y, key.w, key.h);
        ctx.fill();
    }
    ctx.fillStyle = "#ccc";
    for(let key of keys) if(!key.black)
    {    drawkey(key);
    }
    ctx.fillStyle = "#000";
    for(let key of keys) if(key.black)
    {   drawkey(key);
    }
}


export function gui()
{   canvas = document.createElement('canvas');
    canvas.style.width = '100%';
    canvas.style.height = '200px';
    canvas.height = canvash;
    canvas.width = canvasw;
    draw();
    canvas.addEventListener('mousedown', mouseDown);
    canvas.addEventListener('mousemove', mouseMove);
    canvas.addEventListener('mouseup', mouseUp);
    canvas.addEventListener("touchstart", touchStart, false);
    canvas.addEventListener("touchmove", touchMove, false);
    canvas.addEventListener("touchend", touchEnd, false);
    canvas.addEventListener("touchcancel", touchEnd, false);
    return canvas;
}
export function onMidi(func)
{   midiOut = func;
}
