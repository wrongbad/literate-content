let requested = false;
let midiAccess = null;
let midiOut = null;
let midiInputs = [];
let midiDict = {};
let sel = null;

async function updateList()
{   if(!requested)
    {   requested = true;
        midiAccess = await navigator.requestMIDIAccess();
    }
    if(!sel) { return; }
    const inputs = midiAccess.inputs.values();
    let o = 0;
    let oldlen = sel.options.length;
    for(let input of inputs)
    {   o++;
        if(o < oldlen && sel.options[o].value == input.name) { continue; }
        let opt = document.createElement("option");
        opt.value = input.name; // must be string
        opt.text = input.name;
        midiDict[input.name] = input;
        if(o < oldlen) { sel.options[o] = opt; }
        else { sel.add(opt); }
    }
}

function updateMidiOut()
{
    if(!sel) { return; }
    for(let oldin of midiInputs)
    {   oldin.onmidimessage = null;
    }
    midiInputs = [];
    for(let opt of sel.selectedOptions)
    {   let newin = midiDict[opt.value];
        if(!newin) { continue; }
        newin.onmidimessage = midiOut;
        midiInputs.push(newin);
    }
}

export function gui()
{   sel = document.createElement('select');
    let opt = document.createElement("option");
    opt.value = null;
    opt.text = "midi disconnected";
    sel.add(opt);
    
    sel.onchange = updateMidiOut;
    sel.onclick = updateList;
    
    updateList();
    updateMidiOut();
    
    return sel;
}

export function onMidi(func)
{   midiOut = func;
    updateMidiOut();
}
