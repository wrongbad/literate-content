export function slider(midiId, midiOut)
{   let sliderbox = document.createElement('div');
    let slider = document.createElement('input');
    slider.type = 'range';
    slider.oninput = ()=>midiOut([0xb0, midiId, +slider.value])
    Object.assign(slider.style, {
        width: '100px',
        height: '20px',
        left: '10px',
        margin: '0px',
        transform: 'rotate(-90deg)',
        'transform-origin': '50px 50px',
    });
    sliderbox.style.width = '20px';
    sliderbox.style.height = '100px';
    sliderbox.style.display = 'inline-block';
    sliderbox.appendChild(slider);
    return sliderbox;
}
