let audioContext = null;
let startCb = null;

export function gui()
{   let button = document.createElement('a');
    let muted = `audio <i class="fas fa-volume-mute"></i>`; 
    let enabled = `audio <i class="fas fa-volume-up"></i>`; 
    button.innerHTML = muted;
    button.href='#';
    button.style.margin='5px';
    button.onclick = ()=>{
        if(audioContext)
        {   audioContext.close();
            audioContext=null;
            button.innerHTML = muted;
            return;
        }
        audioContext = new AudioContext();
        button.innerHTML = enabled;
        startCb && startCb(audioContext);
    };
    return button;
}

export function onStart(cb)
{   startCb = cb;
}
