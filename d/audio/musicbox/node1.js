// ok maybe I have a golfing problem
var A=Float32Array, r=44100, l=4096, g=60*r,
t=new A(100), b=new A(1<<16), s=new A(g),
seq=s=>[5,0,-7,-5,12,5,0,5,5,12,0,5,0,-7,5,0][s|0],
n2f=n=>27.5*Math.exp(n*.05776),
saw=i=>t[i]=(t[i+2]=(t[i+2]+t[i+1]/r)%1)-0.5,
fil=i=>t[i]=(t[i+3]+=(t[i+1]-t[i+3])*t[i+2]/r),
del=i=>{b[t[i+4]+t[i+2]*r&0xffff]=t[i+1]+t[i+3]*(t[i]=b[t[i+4]]);t[i+4]=t[i+4]+1&0xffff;return t[i];};
t[0]=0.3*r|0;t[1]=.1;t[2]=.2;t[3]=.7;t[53]=0.8;t[62]=3000;
class MusicBoxDemo extends AudioWorkletProcessor
{   process(inputs, outputs, parameters)
    {   for(let i=0;i<outputs[0][0].length;i++)
        {   t[1]+=1/t[0];t[2]+=4/3/t[0];t[3]+=3/4/t[0]; // sequencer steps
        	t[11]=n2f(43+seq((t[1]%16*1.5|0)/1.5)+seq(t[1]/16%16));
        	t[21]=n2f(43+seq((t[2]%16*1.5|0)/1.5)+seq(t[1]/16%16));
        	t[31]=n2f(31+seq((t[3]%16*1.5|0)/1.5)+seq(t[1]/16%16));
        	saw(10);saw(20);saw(30); // base waveforms
        	t[51]=t[10]*t[20]*8/3; // amplitude modulation
        	t[51]=Math.abs(0.5-Math.abs(t[51]+0.25))-0.25; // distortion
        	t[51]+=((t[30]>0)-0.5)/2 // saw-to-square
        	t[52]=2.01/t[11]; // pitch tracking
        	t[61]=del(50); // comb filter
        	outputs[0][0][i]=fil(60)/3; // lowpass
        }
        return true;
    }
}
registerProcessor('musicbox1', MusicBoxDemo);